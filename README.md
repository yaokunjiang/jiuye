## 实训一

## 你们在项目做过哪些配置？优化了工作效率，代码，打包流程，团队协作，项目怎么区分环境。

1. process 是nodejs 全局进程对象 process.env 环境变量

 通过环境变量来区分开发、生产、测试等环境

 npm run serve =》 process.env.NODE_ENV development

 npm run build =》 process.env.NODE_ENV production

 npm run lint =》 自动修复代码规范

 npm run test =》 process.env.NODE_ENV test

 环境配置 baseURl

 .env.[mode]

2. 我们脚手架是基于vuecli做的二次开发，约束语法风格代码规范
   
   eslint：

   .eslintrc.js 文件

   prettier 代码提交校验：

    husky 定义git hooks指定时间执行的函数

    .huskyrc
      pre-commit: commit之前执行hook  执行 lint-staged 指令 配置.lintstagedrc文件

      commit-msg: commit提交时校验m信息格式 commitlint指令 配置commitlint.config.js

   babel优化：

    打包去除consolelog
    第三方包按需加载