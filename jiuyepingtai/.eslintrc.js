module.exports = {
	root: true, // 在根路径下开发校验
	env: {
		node: true, // 环境
	},
	plugins: ['prettier'],
	extends: [
		//检测语法包
		'plugin:vue/vue3-essential',
		'eslint:recommended',
		'@vue/eslint-config-prettier',
	],
	parserOptions: {
		// 解析代码格式
		parser: 'babel-eslint',
	},
	rules: {
		// 自定义校验规则
		// 'no-console': process.env.NODE_ENV === 'production' ? 'error' : 'off',
		'no-debugger': process.env.NODE_ENV === 'production' ? 'error' : 'off',
		'prettier/prettier': ['error'],
	},
};
