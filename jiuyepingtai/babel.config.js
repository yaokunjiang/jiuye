const plugins = [];

plugins.push([
	'import',
	{
		libraryName: 'element-plus',
		customStyleName: name => {
			name = name.slice(3);
			return `element-plus/packages/theme-chalk/src/${name}.scss`;
		},
	},
]);

if (process.env.NODE_ENV === 'production') {
	plugins.push('transform-remove-console');
}

module.exports = {
	presets: ['@vue/cli-plugin-babel/preset'],
	plugins,
};
